using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ChatPanel : MonoBehaviour
{
    const string KEY_SAVE_HISTORY = "KEY_SAVE_HISTORY";

    [Header("Chat Config")]
    public int fontSize;
    public Color colorLeft, colorRight;
    public bool saveHistory;
    public bool autoClearHistory;
    public int autoClearHistoryCache;


    [Header("Chat Scroller")]
    [SerializeField]
    private ChatCellView m_prefabCellView;
    [SerializeField]
    private ScrollRect m_scrollRect;
    [SerializeField]
    private RectTransform m_scrollRectContent;
    [SerializeField]
    private GameObject m_loadingPanel;

    private List<ChatData> m_chatDatas;

    // Start is called before the first frame update
    void Start()
    {
        m_chatDatas = new List<ChatData>();
        m_loadingPanel.gameObject.SetActive(false);
        LoadData();
    }


    /// <summary>
    /// Add newData to list view
    /// </summary>
    /// <param name="chatSide">Left or Right</param>
    /// <param name="text">Content of chat</param>
    public void AddData(ChatData.ChatSide chatSide, string text)
    {
        //Create Object to store chat datas
        ChatData data = new ChatData();
        data.fontSize = fontSize;
        data.text = text;
        data.chatSide = chatSide;
        data.color = chatSide == ChatData.ChatSide.LEFT ? colorLeft : colorRight;

        //Instantiate new cell View
        ChatCellView chatView = Instantiate(m_prefabCellView, m_scrollRectContent);
        //Set Data for new cell View
        chatView.SetData(data);
        //Assign cellView to Object for delete if need
        data.cellView = chatView;

        //Add Object to list
        m_chatDatas.Add(data);
        //Save Datas
        SaveData();

        //Scroll to Bottom of chat
        m_scrollRect.normalizedPosition = new Vector2(0, 0);
        //Call Scroll again to make sure it work
        StopAllCoroutines();
        StartCoroutine(ScrollToBottom());

        //Clear old history if need
        ClearHistory();

    }

    IEnumerator ScrollToBottom()
    {
        yield return new WaitForEndOfFrame();
        m_scrollRect.normalizedPosition = new Vector2(0, 0);
        //Loading done: Turn off loadingPanel
        m_loadingPanel.gameObject.SetActive(false);
    }

    /// <summary>
    /// Remove old data
    /// </summary>
    /// <param name="index">Index of data to remove</param>
    public void RemoveData(int index)
    {
        ChatData dataToRemove = m_chatDatas[index];
        //Destroy cell view
        Destroy(dataToRemove.cellView.gameObject);
        //Remove data from list
        m_chatDatas.RemoveAt(index);
        //Save datas again
        SaveData();
    }

    /// <summary>
    /// Clear history
    /// </summary>
    void ClearHistory()
    {
        if (autoClearHistory)
        {
            //If  autoClearHistory is On
            //Check if Datas is bigger than autoClearHistoryCache
            if (m_chatDatas.Count > autoClearHistoryCache)
            {
                //Get number of datas to remove
                int numberToRemove = m_chatDatas.Count - autoClearHistoryCache;
                //remove Datas
                for (int i = 0; i < numberToRemove; i++)
                {
                    RemoveData(0);
                }
            }
        }
    }

    /// <summary>
    /// Load Data from last time
    /// </summary>
    void LoadData()
    {
        if (saveHistory)
        {
            //Turn on loadingPanel and wait for load last data
            m_loadingPanel.gameObject.SetActive(true);
            //If SaveHistory On:
            //Get Data from PlayerPrefs
            string data = PlayerPrefs.GetString(KEY_SAVE_HISTORY, "");
            //Convert string data to ChatDataSave object:
            ChatDataSave save = JsonUtility.FromJson<ChatDataSave>(data);
            if (save != null && save.datas.Count > 0)
            {
                foreach (var item in save.datas)
                {
                    //Add each data to list view
                    AddData(item.chatSide, item.text);
                }
            }
            else
            {
                //Nothing to load: Turn off loadingPanel
                m_loadingPanel.gameObject.SetActive(false);
            }
        }
        else
        {
            //If SaveHistory Off:
            //Delete Data from PlayerPrefs
            PlayerPrefs.DeleteKey(KEY_SAVE_HISTORY);
            PlayerPrefs.Save();
        }
    }

    /// <summary>
    /// Save chat Data
    /// </summary>
    void SaveData()
    {
        if (saveHistory)
        {
            //If SaveHistory On:
            //Convert current data to json
            ChatDataSave save = new ChatDataSave() { datas = m_chatDatas };
            //And save to PlayerPrefs
            PlayerPrefs.SetString(KEY_SAVE_HISTORY, JsonUtility.ToJson(save));
            PlayerPrefs.Save();
        }
    }

}
