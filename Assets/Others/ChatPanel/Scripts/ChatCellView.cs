using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatCellView : MonoBehaviour
{
    [SerializeField]
    private Text m_txtView;
    [SerializeField]
    private RectTransform m_rectChat;
    [SerializeField]
    private LayoutElement m_layoutElement;

    private ChatData m_Data;


    /// <summary>
    /// Update data to view
    /// </summary>
    /// <param name="data"></param>
    public void SetData(ChatData data)
    {
        m_Data = data;
        m_txtView.fontSize = m_Data.fontSize;
        m_txtView.text = m_Data.text;
        //Set chat panel on left or right:
        switch (m_Data.chatSide)
        {
            case ChatData.ChatSide.LEFT:
                m_rectChat.pivot = new Vector2(0, 1);
                m_rectChat.anchorMax = new Vector2(0, 1);
                m_rectChat.anchorMin = new Vector2(0, 1);
                break;
            case ChatData.ChatSide.RIGHT:
                m_rectChat.pivot = new Vector2(1, 1);
                m_rectChat.anchorMax = Vector2.one;
                m_rectChat.anchorMin = Vector2.one;
                break;
            default:
                break;
        }
        m_rectChat.anchoredPosition = Vector3.zero;
        //Set Color of chat 
        m_rectChat.GetComponent<Image>().color = data.color;

        //Update Size of this cellView
        StartCoroutine(UpdateCellSize());

    }

    IEnumerator UpdateCellSize()
    {
        //Wait for end of frame to make sure UI updated
        yield return new WaitForEndOfFrame();
        m_layoutElement.preferredHeight = m_rectChat.sizeDelta.y;
    }
}
