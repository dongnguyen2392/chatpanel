using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
public class ChatPanelInput : MonoBehaviour
{
    [Header("Config")]
    [SerializeField]
    private ChatData.ChatSide m_chatSide;
    [Header("Reference")]
    [SerializeField]
    ChatPanel m_chatPanel;
    [SerializeField]
    TMP_InputField m_if;
    [SerializeField]
    Button m_btnSend;
    // Start is called before the first frame update
    void Start()
    {
        m_btnSend.onClick.AddListener(OnSendClickEvent);
        m_if.onSubmit.AddListener(OnSubmitEvent);

    }

    private void OnSubmitEvent(string arg0)
    {
        OnSendClickEvent();
    }



    private void OnSendClickEvent()
    {
        if (m_if.text.Length <= 0) return;
        m_chatPanel.AddData(m_chatSide, m_if.text);
        m_if.text = "";
    }



}
