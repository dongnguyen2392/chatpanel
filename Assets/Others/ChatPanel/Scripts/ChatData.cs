using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChatData
{
    public enum ChatSide { LEFT, RIGHT }
    public ChatSide chatSide;
    public string text;
    public int fontSize;
    public Color color;
    public ChatCellView cellView;
}

public class ChatDataSave
{
    public List<ChatData> datas;
}
